import React from 'react';
import classes from './List.module.css';
import ListItem from './ListItem/ListItems';

const List = (props) => {
  return (
    <ul className={classes.list}>
      {props.listItems.map((listItem, index) => (
        <ListItem key={index} listText={listItem} clicked={props.clicked} />
      ))}
    </ul>
  );
};

export default List;
