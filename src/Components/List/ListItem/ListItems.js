import React from 'react';
import classes from './ListItem.module.css';

const ListItem = (props) => {
  return (
    <li className={classes.listItem}>
      {props.listText}
      <span
        className={classes.removeItemButton}
        onClick={() => props.clicked(props.listText)}
      >
        x
      </span>
    </li>
  );
};

export default ListItem;
