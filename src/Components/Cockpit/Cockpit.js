import React from 'react';
import UserInputForm from './UserInputForm/UserInputForm';

const Cockpit = (props) => {
  let userInput = null;

  if (props.showUserInputForm) {
    userInput = <UserInputForm clicked={props.addNewItem} />;
  }

  return userInput;
};

export default Cockpit;
