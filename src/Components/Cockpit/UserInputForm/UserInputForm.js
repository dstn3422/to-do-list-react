import React from 'react';

const UserInputForm = (props) => {
  return (
    <form onSubmit={props.clicked}>
      <input type='text' name='userInput' placeholder='enter new task' />
      <input type='submit' value='Add' />
    </form>
  );
};

export default UserInputForm;
