import React from 'react';
import classes from './AppTitle.module.css';

const AppTitle = () => {
  return (
    <div>
      <div className={classes.sticker} data-text='To do list'>
        <span>To do list</span>
      </div>
    </div>
  );
};

export default AppTitle;
