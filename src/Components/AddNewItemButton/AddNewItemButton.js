import React from 'react';
import classes from './AddNewItemButton.module.css';

const AddNewItemButton = (props) => {
  let style = classes.button;

  if (props.showUserInputForm) {
    style = `${classes.button} ${classes.red}`;
  }

  return (
    <div className={style} onClick={props.clicked}>
      Add new task
    </div>
  );
};

export default AddNewItemButton;
