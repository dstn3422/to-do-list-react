import './App.css';
import React, { Component } from 'react';
import AppTitle from './Components/AppTitle/AppTitle';
import List from './Components/List/List';
import AddNewItemButton from './Components/AddNewItemButton/AddNewItemButton';
import Cockpit from './Components/Cockpit/Cockpit';

class App extends Component {
  state = {
    listItems: [],
    showUserInputForm: false,
  };

  addNewItem = (event) => {
    event.preventDefault();
    const newListItems = this.state.listItems.slice();
    newListItems.push(event.target.userInput.value);
    this.setState({ listItems: newListItems });
  };

  toggleUserInput = () => {
    const userInputVisibile = this.state.showUserInputForm;
    this.setState({ showUserInputForm: !userInputVisibile });
  };

  removeItem = (text) => {
    const oldListItems = this.state.listItems.slice();
    oldListItems.splice(oldListItems.indexOf(text), 1);
    this.setState({ listItems: oldListItems });
  };

  render() {
    return (
      <div className='App'>
        <AppTitle />
        <Cockpit
          showUserInputForm={this.state.showUserInputForm}
          addNewItem={this.addNewItem}
        />
        <AddNewItemButton
          clicked={this.toggleUserInput}
          showUserInputForm={this.state.showUserInputForm}
        />
        <List listItems={this.state.listItems} clicked={this.removeItem} />
      </div>
    );
  }
}

export default App;
